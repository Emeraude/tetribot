# Tetribot

A discord bot to play tetris with.

![Demo video](./demo.gif)

## Setup

If you want to invite this bot to you discord server, you just have to [click here](https://discord.com/oauth2/authorize?client_id=776234694798606377&scope=bot). You can then launch the game with to the `!tetris` command.

If you rather prefer to have your own version of the bot running, run the following commands:

```sh
git clone https://gitlab.com/Emeraude/tetribot.git
cd tetribot
npm ci
cp config.json.example config.json
env API_KEY="PUT YOUR API KEY HERE" npm start
```

## Limitations

Since Discord is limiting us to 5 API calls every 5 seconds, the game is really slow, and its speed isn't increasing as time passes. This limitation also prevents the game from being launched several times on the same Discord server.  
Another limitation is the 2000 characters per message one, so keep that in mind if you want to mess up with the board size.

## Why did you make this monstruosity?

Why should I have not do it?
