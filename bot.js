#!/usr/bin/env node

const Discord = require('discord.js')
const config = require('./config.json')
const client = new Discord.Client()

const blocks = [
  {
    // Empty left side to make a more centered rotation
    color: '⬜',
    shape: [[0, 1],
            [0, 1],
            [0, 1],
            [0, 1]]
  }, {
    color: '🟨',
    shape: [[1, 1],
            [1, 1]]
  }, {
    color: '🟥',
    shape: [[0, 1, 1],
            [1, 1, 0]]
  }, {
    color: '🟩',
    shape: [[1, 1, 0],
            [0, 1, 1]]
  }, {
    color: '🟧',
    shape: [[1, 0],
            [1, 0],
            [1, 1]]
  }, {
    color: '🟦',
    shape: [[0, 1],
            [0, 1],
            [1, 1]]
  }, {
    color: '🟪',
    shape: [[1, 0],
            [1, 1],
            [1, 0]]
  }
]

const scores = config.game.scores || [0, 40, 100, 300, 1200]

const gameOverString = [':regional_indicator_g:',
                        ':regional_indicator_a:',
                        ':regional_indicator_m:',
                        ':regional_indicator_e:',
                        ':regional_indicator_o:',
                        ':regional_indicator_v:',
                        ':regional_indicator_e:',
                        ':regional_indicator_r:']

const nextShapeString = [':regional_indicator_n:',
                         ':regional_indicator_e:',
                         ':regional_indicator_x:',
                         ':regional_indicator_t:']

class Tetris {
  #map
  #width
  #height
  #player
  #score
  #tick
  #movingBlock
  #nextBlock
  #gameOver

  constructor(player) {
    this.#player = player
    this.#score = 0
    this.#tick = config.game.tick || 1500
    this.#gameOver = false
    this.#height = config.game.height || 15
    this.#width = config.game.width || 10
    this.#map = []
    for (let i = 0; i < this.#height; ++i) {
      const row = []
      for (let j = 0; j < this.#width; ++j)
        row.push('⬛')
      this.#map.push(row)
    }
    this.generateShape()
  }

  generateShape() {
    if (this.#nextBlock === undefined) {
      const r = blocks[Math.floor(Math.random() * blocks.length)]
      this.#nextBlock = JSON.parse(JSON.stringify(r))
    }
    this.#movingBlock = {
      x: parseInt(this.#width / 2) - 1,
      y: 0,
      color: this.#nextBlock.color,
      shape: this.#nextBlock.shape
    }
    const r = blocks[Math.floor(Math.random() * blocks.length)]
    this.#nextBlock = JSON.parse(JSON.stringify(r))
    if (this.checkCollide(0, 0) === true)
      this.#gameOver = true
  }

  display() {
    let map = JSON.parse(JSON.stringify(this.#map))
    const block = this.#movingBlock
    for (let i in block.shape) {
      for (let j in block.shape[i])
        if (block.shape[i][j] != 0)
          map[parseInt(i) + block.y][parseInt(j) + block.x] = block.color
    }
    if (this.#gameOver) {
      for (let i in gameOverString) {
        map[parseInt(this.#height / 2)][Math.max(0, parseInt(i) + ((this.#width - gameOverString.length) / 2))] = gameOverString[i]
      }
    }

    for (let i in nextShapeString) {
      map[0].push(nextShapeString[i])
    }
    for (let i in this.#nextBlock.shape) {
      i = parseInt(i)
      map[i + 1].push('⬛')
      for (let j in this.#nextBlock.shape[i]) {
        map[i + 1].push(this.#nextBlock.shape[i][j] == 0 ? '⬛': this.#nextBlock.color)
      }
    }

    map = map.map(row => row.join('')).join('\n')
    return this.#player + '\'s score: '
      + this.#score + '\n'
      + map
  }

  checkCollide(h, v) {
    const block = this.#movingBlock
    let map = JSON.parse(JSON.stringify(this.#map))
    for (let i in block.shape) {
      i = parseInt(i)
      for (let j in block.shape[i]) {
        j = parseInt(j)
        if (block.shape[i][j] != 0
            && (i + block.y + v >= this.#height
                || j + block.x + h >= this.#width
                || map[i + block.y + v][j + block.x + h] !== '⬛'))
          return true
      }
    }
    return false
  }

  doLogic() {
    const block = this.#movingBlock
    if (this.checkCollide(0, +1) === false)
      block.y += 1
    else {
      for (let i in block.shape) {
        for (let j in block.shape[i]) {
          if (block.shape[i][j] != 0) {
            this.#map[parseInt(i) + block.y][parseInt(j) + block.x] = block.color
          }
        }
      }
      this.generateShape()
    }
    let nbLines = 0
    for (let i in this.#map) {
      if (this.#map[i].reduce((acc, val) => acc + (val !== '⬛' ? 1 : 0), 0) === this.#width) {
        for (let j = i; j > 0; --j) {
          this.#map[j] = this.#map[j - 1]
          this.#map[0] = []
          for (let j = 0; j < this.#width; ++j)
            this.#map[0].push('⬛')
        }
        nbLines += 1
      }
    }
    this.#score += scores[Math.min(nbLines, scores.length - 1)]
  }

  input(input) {
    switch (input) {
    case '⬅️':
      if (this.checkCollide(-1, 0) === false)
        this.#movingBlock.x -= 1
      break
    case '➡️':
      if (this.checkCollide(+1, 0) === false)
        this.#movingBlock.x += 1
      break
    case '⬇️':
      while (this.checkCollide(0, +1) === false)
        this.#movingBlock.y += 1
      break
    case '🔄':
      const oldShape = JSON.parse(JSON.stringify(this.#movingBlock.shape))
      this.#movingBlock.shape = []
      for(let i = 0; i < oldShape[0].length; i++) {
        let row = oldShape.map(e => e[i]).reverse()
        this.#movingBlock.shape.push(row)
      }
      if (this.checkCollide(0, 0) === true) {
        this.#movingBlock.shape = oldShape
      }
      break
    }
  }

  getTick() {
    return this.#tick
  }

  isOver() {
    return this.#gameOver
  }
}

client.once('ready', () => {
  const gameGuilds = {}
  client.on('message', origMessage => {
    if (origMessage.content === (config.game.command || '!tetris')) {
      if (gameGuilds[origMessage.channel.guild.id] === undefined)
        gameGuilds[origMessage.channel.guild.id] = 0
      if (gameGuilds[origMessage.channel.guild.id] < (config.game.simultaneous_games || 1)) {
        gameGuilds[origMessage.channel.guild.id] += 1
        origMessage.channel.send('Okay ' + origMessage.author.toString() + ', let\'s do this!\nLoading...')
          .then(async message => {
            await message.react('⬅️')
            await message.react('➡️')
            await message.react('⬇️')
            await message.react('🔄')

            const game = new Tetris(origMessage.author.toString())

            const collector = message.createReactionCollector((_, user) => user.id === origMessage.author.id, {dispose: true})
            collector.on('collect', ((e) => game.input(e._emoji.name)))
            collector.on('remove', ((e) => game.input(e._emoji.name)))

            function autoDisplay() {
              game.doLogic()
              message.edit(game.display())
              if (!game.isOver()) {
                setTimeout(autoDisplay, game.getTick())
              } else {
                gameGuilds[origMessage.channel.guild.id] -= 1
              }
            }

            setTimeout(autoDisplay, game.getTick())
          })
      } else {
        origMessage.channel.send('Sorry ' + origMessage.author.toString() + ', there is already one game running.')
      }
    }
  })
})

client.login(process.env.API_KEY || config.api_key)
